# Lab 0: Getting Started #

In this lab, we focus on getting setup with the tools that we will be using for the other labs.

1. [Setting up Google Compute Engine for running our virtual machines](google-cloud.md)
2. [Generating SSH keys for secure access to remote resources](ssh-keys.md)
3. [Cloning and pushing Git repositories with Bitbucket](bitbucket-git.md)

