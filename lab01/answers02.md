> Q: Compile and run ```tracer2``` a few times and observe the results.
>
> 1. Where are ```buffer``` and ```array``` relative to stuff?

Answer:
address(buffer) < address(array) < address(stuff)


> 2. What happens to the address of ```stuff``` each time the function recurses?

Answer:
address of stuff increases each time the function recurses


> 3. What is going on here? Explain why we get these behaviors in (1) and (2) above.

Answer:

Heap grows up.

In the code, buffer is declared before array, and array is declared before stuff.
Therefore, we see that address increase in question 1.

Also, the address of stuff increase as the function recurses, which shows that heap grows up.


