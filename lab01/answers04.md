> 1. How many bits is your system ASLR randomizing for the stack? How can you tell? Pick one of the stack variables and report the highest address that you observed for that variable, and the lowest address for the same variable.

Answer:
Up to 48 bits. Because the system is 64 bits.
Highest Value: 0xffeace40
Lowest Value: 0xff827db0
Bits: 23


> 2. How many bits of randomizztion are bein gapplied to your heap? As above, give the highest and lowest addresses that you observed for one of the heap variables.

Answer:
Highest Value: 0x56d14570
Lowest Value: 0x577b2570
Bits: 21


> 3. How many bits for the code? As above, report the highest and lowest addresses taht you observed for a function in the program?

Answer:
Highest Value: 0x565ee6af
Lowest Value: 0x5655e6af
Bits: 20
