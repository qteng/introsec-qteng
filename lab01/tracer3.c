#include <stdio.h>
#include <malloc.h>

int A;
int B;

int function(int depth) {
    int rc;
    char buf[5];
    char *stuff = (char *) malloc(16*sizeof(char));

    printf("rc = %p\n", &rc);
    printf("buf = %p\n", &buf);

    printf("stuff = %p\n", stuff);

    if(depth < 10)
        rc = function(depth+1);
	else
		rc = 0;

	free(stuff);

    return rc;
}

int main(int argc, char* argv[]) {

    printf("main = %p\n", main);
    printf("function = %p\n", function);
    printf("printf = %p\n", printf);

    int x;
    char *buffer = (char *) malloc(128*sizeof(char));
    int  *array = (int *) malloc(128*sizeof(int));

    printf("x = %p\n", &x);

    printf("buffer = %p\n", buffer);
    printf("array = %p\n", array);

    x = function(10);

    return 0;
}
