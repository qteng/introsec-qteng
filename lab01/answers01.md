> Q: Run tracer1b a few times and observe the results. Are the memory locations for these three variables in any certain order? In other words, are some of the variables consistently stored at higher (or lower) addresses than the others? What's going on here and why?

Answer:

From my result, 
	address(x) > address(buf) > address(rc)

First of all, stack grows down. Thereofore, the stack frame of function is under that of the main function.
This is why the address of x is the greatest.

Then, in function, rc is declared before buf, which means rc is var1, and buf is var2. 
Based on the textbook, it should be that address(var1) > address(var2).
But the C language does not impose specific memory order here.  
Therefore, it is more implementation depended.
In my result, buf's address is greater than that of rc.

