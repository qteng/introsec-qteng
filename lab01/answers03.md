> Compile and run ```tracer3``` and observe the results.
> 
> 1. Each time you run the program, what do all of the code addresses have in common?

Answer:
All of the addresses remain the same no matter how many times I ran the code.


> 2. Why does (1) happen? Explain what you're seeing.

Answer:
Because ASLR is currently disabled.
By disabling ASLR, the base address of an executable and the position of stack and heap are positioned in a fix space. Therefore, running the code multiple times does not change the addresses of the functions, stack and heap.


> 3. Now re-run the program several times. Do the addresses change from run to run? If so, how do they change each time?

Answer:
I did not see any address change from running the code.


