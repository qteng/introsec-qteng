# Lab 2: Smashing the Stack #

## Configuring Your VM

Before we can run classic exploit code on a modern system, we need to turn off the basic set of protections that have been put in place.  DEP and stack canaries can be disabled at compile time for each of our target programs.  But for ASLR, we need to modify one of the system-wide settings.  Just like we did at the beginning of Lab 1, we can disable ASLR with the following command:

```console
user@host:lab02$ sudo sysctl -w kernel.randomize_va_space=0
```

## Pulling the latest skeleton code from introsec-public

Next, we need to fetch the latest updates from the [introsec-public](https://bitbucket.org/introsec-pathak/introsec-fall2020/src/master/) Git repo.  This will pull in the baseline "skeleton" code for Lab 2.

Assuming you cloned from the `introsec-fall2020` repo into a local folder called `homework`, you can pull from it using the identifier `origin` like this: 

```console
user@host:~$ cd homework
user@host:homework$ git pull origin master
```

As always for this course, we'll be working on the `master` branch.

## Compiling the Baseline Code

The baseline code for Lab 2 comes with a Makefile to automate the build process.  Type `make` to compile all the C programs, including the target programs and your attack code, with the proper set of compiler flags to disable DEP and StackGuard on the target programs.

```console
user@host:lab02$ make
```

## Make a PDF document. 
Make a PDF document with the name: "Lab02_LastName.pdf", it should have a screenshot with the successful execution of each program along with a very brief writeup on how you were able to Capture the Flag. Push the Pdf file onto your repo. 


## Problem 1: Reverse Engineering with GDB (Part 1)

The program [target01](target01.c) asks you to guess a secret "magic" number. If you guess correctly, the program will spawn a shell for you.  Your "attack" program should use this shell to print out the contents of the file `flag01.txt` as the last line of its output.

_Hint_: On Linux, the `cat` program will read the contents of a file and print them to its standard output.

You have been provided with a copy of the program's source code in [target01.c](target01.c), but the magic number has been censored.  Use `gdb` to examine the program's memory as it runs.  Extract the magic value.

_Hint: Disassemble the code for the function `check_guess`.  It compares your input value to a hard-coded constant.  That constant is the magic number.  You can run the program under gdb using the command:_

```console
user@host:lab02$ gdb ./target01
```

Once you have reverse-engineered the target program to obtain the magic number, you can fill in [attack01.c](attack01.c) with a simple program to execute the "attack".  Your program should (1) feed the magic value into the program to obtain the shell, then (2) uses the shell to print the flag.  (It's OK, and normal, if you simply hard-code the magic number into your program.)

Your program should work when its output is piped in to the victim program's standard input, like this:

```console
user@host:lab02$ ./attack01 | ./target01
```

Alternatively, the Makefile includes a recipe for running this command.  The Makefile also knows to automatically re-compile your attack program if its source file has been modified since the last compile.

```console
user@host:lab02$ make run01
```


To receive credit for this problem, 

1) turn in your attack code in [attack01.c](attack01.c) . 

2) Take a screenshot of the successful execution of the "make run01" or "./attack01 | ./target01" . 

3) Briefly mention how did you figure out the "MAGICNUM". 


## Tips for Writing Your Attack Code

I received a very good question from a student who was working on Problem 1 and having trouble getting access to the shell from the attack program. When the student typed in the magic number manually, the shell spawned just as expected. But when they printed the same magic number from the attack program, the victim printed its "success" message, but no shell prompt appeared. What happened?

The problem happens because, unlike manual typing, your attack program can deliver all of its output *extremely* quickly. So it's easy for the victim program to slurp up all the bytes into some input buffer before the shell can even get started. If the shell starts and there's no input left to consume, it simply closes. We don't want that!

We need the victim program to get some of our output, and then we need the rest of our output to go to the shell.

The trick that I've found works the best is to use a combination of `fflush()` and `sleep()` to (1) make sure that all the data that we output for the victim program has actually been sent through the pipe, and then (2) give the new shell a second or two to start up. Then anything else we write should go to the shell. So, for example, our attack program might look something like this:

```c
  puts(attack_payload);  // This is the data that we want to go to the victim program
  fflush(stdout);        // Make sure all of the data above has made it through the pipe
  sleep(2);              // Wait for the shell to spawn...
  puts(shell_command);   // This is the data that we want to feed into the shell
```


## Problem 2: Reverse Engineering with GDB (Part 2)
The program in Problem 2 is very similar to the code from Problem 1. Only now the secret value is stored in the program's **data section**, rather than **on the stack**.

Once again, your task is to first run the victim program in GDB and extract its magic value.  Then you can re-run the program without gdb and feed it the magic number to obtain the shell.

Fill in [attack02.c](attack02.c) with a simple program to feed the "magic" value to the victim program and obtain the shell and print the flag, just like you did for Problem 2.  Your "attack" should cause the target program to print out the contents of the file `flag02.txt` as the last line of its output.

You can run this one just like you did for Problem 1.

```console
user@host:lab02$ make run02
```

1) Turn in your code in [attack02.c](attack02.c) when you are done.

2) Take a screenshot of the successful execution of the "make run02" or "./attack02 | ./target02" . 

3) Briefly mention how did you figure out the value of "magic". 


## Problem 3: Buffer Overflow onto an Int
The program in Problem 3 stores an integer, `x`, on the stack in the vulnerable function `vuln()`. It assigns `x` the value 0. In order to obtain the flag, you must force `x` to take on a different value.

First analyze the program in `gdb` to extract the magic value for x that will cause the program to spawn a shell. Then craft an input that overflows the buffer `buf` and clobbers `x` with this magic value.  Your attack should then use the resulting shell to print out the contents of the file `flag03.txt` as the last line of its output.

1) Turn in your code in [attack03.c](attack03.c) when you are done.

2) Take a screenshot of the successful execution of the "make run03" or "./attack03 | ./target03" . 

3) Briefly mention about the steps you took that got you  success. 


## Don't Forget to Turn In Your Code along with a PDF based report.
Remember to run **git add** on all of your source files, then **git commit**, then finally **git push -u personal master**.  If you have made any changes to your personal Git repo, you may need to **pull** them down into your local working copy before you can push any new changes back up into the repo.

```console
user@host:lab02$ git add attack*.c
user@host:lab02$ git commit -m "Done with Lab 2"
user@host:lab02$ git pull personal master
user@host:lab02$ git push personal master
```


