#!/bin/env python3

import os
import sys
import random

# Different random seeds for each user
username = os.environ['USER']
if 'INTROSEC_USER' in os.environ:
    username = os.environ['INTROSEC_USER']

# Different random seeds for each use case
seed_string = username
if len(sys.argv) > 1:
    seed_string += sys.argv[1]

random.seed(seed_string)
magic = random.randint(0,2**31-1)

print("0x%08x" % magic)
